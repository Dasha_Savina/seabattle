import java.util.ArrayList;


/**
 * Объект класса "Корабль"  хранит информацию о местоположении корабля
 * и  обрабатывает выстрел пользователя.
 *
 *
 * Created by Dashka on 21.09.2017.
 */
public class Ship {
    private static ArrayList<String> location = new ArrayList<>();//сделать приватном

    /**
     * Метод для присваивания начального положения коробля.
     *
     * @param с координаты корабля.
     */
    public void setLocation(int с) {
        location.add(Integer.toString(с));
        location.add(Integer.toString(с + 1));
        location.add(Integer.toString(с + 2));
    }

    /**
     * Метод для обработки выстрела пользователя,
     * при выстреле выводит сообщение пользователю "Ранил","Ты победил ,поздравляю!" или "Мимо".
     *
     * @param shot координата выстрела ,который принимает значение введеное пользователем с клавиатуры.
     */

    public String shoots(int shot) {
        int index = location.indexOf(Integer.toString(shot));
        String result="Мимо";
        if (index != -1) {
            location.remove(index);
            if (location.isEmpty()) {
                result= "Убил";
            } else {
                result = "Ранил";
            }
        }
        return result;
    }

    /**
     * Метод toString() выводит информацию о текущей позиции корабля.
     *
     * @return возвращает положение корабля.
     */

    @Override
    public String toString() {
        return "Расположение коробля = " + location +
                '}';
    }
}






