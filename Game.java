
import java.util.Scanner;

/**
 *  Основной метода для запуска игры морской бой,
 *  пользователь вводит одну координату
 *  и при попадении выводится соответствующий результат
 *
 *
 * Created by Dashka on 21.09.2017.
 */
public class Game {
static final int c = (int) (Math.random()*8);//сделала константу
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        //int c = (int) (Math.random() * 8);//сделать константой
        Ship ship = new Ship();
        ship.setLocation(c);
        System.out.println(ship.toString());
        int attempt = 0;

        String result;
        do {
            System.out.println("Введите координату ");
            int shot = scanner.nextInt();
            attempt++;
            result = ship.shoots(shot);
            System.out.println(result);
        } while (!result.equals("Убил"));//сделать чтобы стравнивало строку
        System.out.println("Ты победил,поздравляю!");
        System.out.println("Количество попыток = " + attempt);
    }
}
